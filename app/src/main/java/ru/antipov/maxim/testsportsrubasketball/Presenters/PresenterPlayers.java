package ru.antipov.maxim.testsportsrubasketball.Presenters;

import android.content.Context;

import java.util.List;

import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;
import ru.antipov.maxim.testsportsrubasketball.Models.PlayersModel;
import ru.antipov.maxim.testsportsrubasketball.Views.PlayersView;

public class PresenterPlayers {
    private PlayersModel model;
    private PlayersView view;

    public PresenterPlayers() {this.model = new PlayersModel();}
    public void attachView(PlayersView view) {this.view = view;}

    public void addPlayers(Context context) {
        model.loadPlayers(context, new PlayersModel.LoadCallback() {
            @Override
            public void onLoad(List<Player> players) {
                view.addPlayers(players);
            }

            @Override
            public void onError(String error) {
                view.toastMessage(error);
                view.addPlayers(null);
            }
        });
    }

    public void prefNextFragment(Player player) {
        view.prefNextFragment(player);
    }
}

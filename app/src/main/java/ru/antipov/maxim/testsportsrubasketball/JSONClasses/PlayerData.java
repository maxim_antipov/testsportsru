package ru.antipov.maxim.testsportsrubasketball.JSONClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PlayerData {
    @SerializedName("data")
    @Expose
    private ArrayList<Player> player;

    public List<Player> getPlayer() {
        return player;
    }
}

package ru.antipov.maxim.testsportsrubasketball.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.antipov.maxim.testsportsrubasketball.Adapter.PlayerAdapter;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;
import ru.antipov.maxim.testsportsrubasketball.PlayerBasketballActivity;
import ru.antipov.maxim.testsportsrubasketball.Presenters.PresenterFavoritesPlayers;
import ru.antipov.maxim.testsportsrubasketball.Presenters.PresenterPlayers;
import ru.antipov.maxim.testsportsrubasketball.R;
import ru.antipov.maxim.testsportsrubasketball.Views.FavoritesPlayerView;

public class FavoritesPlayersFragment extends Fragment implements FavoritesPlayerView {
    public static FavoritesPlayersFragment newInstance() {
        return new FavoritesPlayersFragment();
    }
    private final String TAG = "FavoritesPlayersFragment";

    @BindView(R.id.rvPlayer) RecyclerView rvPlayer;
    @BindView(R.id.loadingMain) View loading;

    private PlayerAdapter playerAdapter;
    private PresenterFavoritesPlayers presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("Fragment1", "onCreate");

        presenter = new PresenterFavoritesPlayers();
        presenter.attachView(this);

        presenter.addPlayers(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.players_fragment, container, false);
        ButterKnife.bind(this, v);

        Log.d("Fragment1", "onCreateView");

        rvPlayer.setLayoutManager(new LinearLayoutManager(getContext()));

        return v;
    }

    @Override
    public void addPlayers(List<Player> players) {
        if(playerAdapter == null) {
            if(players != null) {
                playerAdapter = new PlayerAdapter(players, rvPlayer, getContext());

                playerAdapter.setOnClickListenerRVAdepter(new PlayerAdapter.setOnClickListenerBtnMoreInfo() {
                    @Override
                    public void onClickMoreInfo(Player player) {
                        presenter.prefNextFragment(player);
                    }
                });

                rvPlayer.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);

                rvPlayer.setAdapter(playerAdapter);
            }
        } else {
            playerAdapter.removeNull();
            Log.d("counting", "addPlayers");

            if(players != null) {
                playerAdapter.addIndexPlayers(playerAdapter.getItemCount(), players);
                playerAdapter.notifyItemRangeChanged(playerAdapter.getItemCount(), players.size());
            }
        }
    }

    @Override
    public void prefNextFragment(Player player) {
        if (getActivity() != null) {;
            Intent intent = PlayerBasketballActivity.newIntent(getActivity(), player);
            startActivity(intent);
        }
    }

    @Override
    public void toastMessage(String message) {
        if(getContext() != null) {
            Log.d(TAG, message);
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }
}

package ru.antipov.maxim.testsportsrubasketball;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import ru.antipov.maxim.testsportsrubasketball.Fragments.PlayerFragment;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;

public class PlayerBasketballActivity extends SingleFragmentActivity {
    private static final String ARG_PLAYER = "PLAYER";

    public static Intent newIntent(Context packageContext, Player player) {
        Intent intent = new Intent(packageContext, PlayerBasketballActivity.class);
        intent.putExtra(ARG_PLAYER, player);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        Player player = (Player) getIntent().getParcelableExtra(ARG_PLAYER);
        return PlayerFragment.newInstance(player);
    }
}

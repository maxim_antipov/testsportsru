package ru.antipov.maxim.testsportsrubasketball;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.PlayerData;
import rx.Observable;

public interface Api {
    @GET("players")
    Observable<PlayerData> getPlayers(@Query("page")     String page,
                                      @Query("per_page") String per_page);
    @GET("players")
    Call<PlayerData> searchPlayers(@Query("search") String search_name);
}

package ru.antipov.maxim.testsportsrubasketball.JSONClasses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Team implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("abbreviation")
    @Expose
    private String abbreviation;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("conference")
    @Expose
    private String conference;

    @SerializedName("division")
    @Expose
    private String division;

    @SerializedName("full_name")
    @Expose
    private String full_name;

    @SerializedName("name")
    @Expose
    private String name;

    public Team() {
        id           = -1;
        abbreviation = "";
        city         = "";
        conference   = "";
        division     = "";
        full_name    = "";
        name         = "";
    }

    public int getId() {
        return id;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getCity() {
        return city;
    }

    public String getConference() {
        return conference;
    }

    public String getDivision() {
        return division;
    }

    public String getFullName() {
        return full_name;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setConference(String conference) {
        this.conference = conference;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public void setFullName(String full_name) {
        this.full_name = full_name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(abbreviation);
        parcel.writeString(city);
        parcel.writeString(conference);
        parcel.writeString(division);
        parcel.writeString(full_name);
        parcel.writeString(name);
    }

    public static final Parcelable.Creator<Team> CREATOR = new Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel parcel) {
            return new Team(parcel);
        }

        @Override
        public Team[] newArray(int i) {
            return new Team[i];
        }
    };

    private Team(Parcel parcel) {
        id           = parcel.readInt();
        abbreviation = parcel.readString();
        city         = parcel.readString();
        conference   = parcel.readString();
        division     = parcel.readString();
        full_name    = parcel.readString();
        name         = parcel.readString();
    }
}

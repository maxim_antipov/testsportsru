package ru.antipov.maxim.testsportsrubasketball.Models;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerBaseHelper;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema;
import ru.antipov.maxim.testsportsrubasketball.DataBase.ReadDataBase;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Team;
import ru.antipov.maxim.testsportsrubasketball.Utils.Utils;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FavoritesPlayersModel {
    public interface LoadCallback {
        void onLoad(List<Player> players);
        void onError(String error);
    }

    private List<Player> players;

    public FavoritesPlayersModel() {
        players = new ArrayList<>();
    }

    public void loadPlayers(final Context context, final LoadCallback loadCallback) {
        Observable.create(new ReadDataBase(context).readFavoritesPlayer())
                .subscribeOn(Schedulers.io())
                .onBackpressureBuffer()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Player>() {
                    @Override
                    public void onCompleted() {
                        loadCallback.onLoad(players);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadCallback.onError(e.toString());
                    }

                    @Override
                    public void onNext(Player player) {
                        players.add(player);
                        Log.d("counting", "players.size() = " + players.size() + " name = " + Thread.currentThread().getName());
                    }
                });
    }
}

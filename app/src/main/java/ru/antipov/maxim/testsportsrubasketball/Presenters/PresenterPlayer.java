package ru.antipov.maxim.testsportsrubasketball.Presenters;

import android.content.Context;

import ru.antipov.maxim.testsportsrubasketball.Models.PlayerModel;
import ru.antipov.maxim.testsportsrubasketball.Views.PlayerView;

public class PresenterPlayer {
    private PlayerModel model;
    private PlayerView view;

    public PresenterPlayer() {this.model = new PlayerModel();}
    public void attachView(PlayerView view) {this.view = view;}

    public void addFavorites(Context context, int id_player) {
        model.addFavoritePlayer(context, new PlayerModel.LoadCallback() {
            @Override
            public void onLoad() {
                view.toastMessage("Add in Favorite");
            }
        }, id_player);
    }
}

package ru.antipov.maxim.testsportsrubasketball.Views;

import java.util.List;

import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;

public interface PlayersView {
    void addPlayers(List<Player>players);
    void prefNextFragment(Player player);
    void toastMessage(String message);
}

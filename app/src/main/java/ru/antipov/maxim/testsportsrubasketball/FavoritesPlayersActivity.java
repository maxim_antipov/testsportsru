package ru.antipov.maxim.testsportsrubasketball;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import ru.antipov.maxim.testsportsrubasketball.Fragments.FavoritesPlayersFragment;

public class FavoritesPlayersActivity extends SingleFragmentActivity {
    public static Intent newIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, FavoritesPlayersActivity.class);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        return FavoritesPlayersFragment.newInstance();
    }
}

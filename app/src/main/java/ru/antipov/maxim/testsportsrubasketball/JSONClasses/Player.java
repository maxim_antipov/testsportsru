package ru.antipov.maxim.testsportsrubasketball.JSONClasses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Player implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("first_name")
    @Expose
    private String first_name;

    @SerializedName("last_name")
    @Expose
    private String last_name;

    @SerializedName("position")
    @Expose
    private String position;

    @SerializedName("team")
    @Expose
    private Team team;

    public Player() {
        id         = -1;
        first_name = "";
        last_name  = "";
        position   = "";
        team       = new Team();
    }

    public Player(Player player) {
        id         = player.id;
        first_name = player.first_name;
        last_name  = player.last_name;
        position   = player.position;
        team       = player.team;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public String getPosition() {
        return position;
    }

    public Team getTeam() {
        return team;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(first_name);
        parcel.writeString(last_name);
        parcel.writeString(position);
        parcel.writeParcelable(team, i);
    }

    public static final Parcelable.Creator<Player> CREATOR = new Creator<Player>() {
        @Override
        public Player createFromParcel(Parcel parcel) {
            return new Player(parcel);
        }

        @Override
        public Player[] newArray(int i) {
            return new Player[i];
        }
    };

    private Player(Parcel parcel) {
        id = parcel.readInt();
        first_name = parcel.readString();
        last_name = parcel.readString();
        position = parcel.readString();
        team = parcel.readParcelable(Player.class.getClassLoader());
    }
}

package ru.antipov.maxim.testsportsrubasketball.Presenters;

import android.content.Context;

import java.util.List;

import ru.antipov.maxim.testsportsrubasketball.Fragments.FavoritesPlayersFragment;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;
import ru.antipov.maxim.testsportsrubasketball.Models.FavoritesPlayersModel;
import ru.antipov.maxim.testsportsrubasketball.Views.FavoritesPlayerView;

public class PresenterFavoritesPlayers {
    private FavoritesPlayersModel model;
    private FavoritesPlayerView view;

    public PresenterFavoritesPlayers() {this.model = new FavoritesPlayersModel();}
    public void attachView(FavoritesPlayerView view) {this.view = view;}

    public void addPlayers(Context context) {
        model.loadPlayers(context, new FavoritesPlayersModel.LoadCallback() {
            @Override
            public void onLoad(List<Player> players) {
                view.addPlayers(players);
            }

            @Override
            public void onError(String error) {
                view.toastMessage(error);
                view.addPlayers(null);
            }
        });
    }

    public void prefNextFragment(Player player) {
        view.prefNextFragment(player);
    }
}

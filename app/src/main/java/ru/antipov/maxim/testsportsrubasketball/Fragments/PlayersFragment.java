package ru.antipov.maxim.testsportsrubasketball.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.antipov.maxim.testsportsrubasketball.Adapter.PlayerAdapter;
import ru.antipov.maxim.testsportsrubasketball.FavoritesPlayersActivity;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;
import ru.antipov.maxim.testsportsrubasketball.PlayerBasketballActivity;
import ru.antipov.maxim.testsportsrubasketball.Presenters.PresenterPlayers;
import ru.antipov.maxim.testsportsrubasketball.R;
import ru.antipov.maxim.testsportsrubasketball.Views.PlayersView;

public class PlayersFragment extends Fragment implements PlayersView {
    private static final String TAG = "PlayersFragment";

    @BindView(R.id.rvPlayer)    RecyclerView rvPlayer;
    @BindView(R.id.loadingMain) View loading;

    private PresenterPlayers presenter;
    private PlayerAdapter playerAdapter;

    public static PlayersFragment newInstance() {
        return new PlayersFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Log.d(TAG, "onCreate");

        presenter = new PresenterPlayers();
        presenter.attachView(this);

        presenter.addPlayers(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.players_fragment, container, false);
        //rlPlayer = (RecyclerView) v.findViewById(R.id.rlPlayer);
        ButterKnife.bind(this, v);

        rvPlayer.setLayoutManager(new LinearLayoutManager(getContext()));

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_players_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorites:
                if (getActivity() != null) {;
                    Intent intent = FavoritesPlayersActivity.newIntent(getContext());
                    startActivity(intent);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void addPlayers(List<Player> players) {
        if(playerAdapter == null) {
            if(players != null) {
                playerAdapter = new PlayerAdapter(players, rvPlayer, getContext());

                playerAdapter.setOnClickListenerRVAdepter(new PlayerAdapter.setOnClickListenerBtnMoreInfo() {
                    @Override
                    public void onClickMoreInfo(Player player) {
                        presenter.prefNextFragment(player);
                    }
                });

                playerAdapter.setOnBeginLoadingRVAdapter(new PlayerAdapter.setOnBeginLoading() {
                    @Override
                    public void beginLoading() {
                        presenter.addPlayers(getContext());
                    }
                });

                rvPlayer.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);

                rvPlayer.setAdapter(playerAdapter);
            }
        } else {
            playerAdapter.removeNull();

            if(players != null) {
                playerAdapter.addIndexPlayers(playerAdapter.getItemCount(), players);
                playerAdapter.notifyItemRangeChanged(playerAdapter.getItemCount(), players.size());
            }
        }
    }

    @Override
    public void prefNextFragment(Player player) {
        if (getActivity() != null) {;
            Intent intent = PlayerBasketballActivity.newIntent(getActivity(), player);
            startActivity(intent);
        }
    }

    @Override
    public void toastMessage(String message) {
        if(getContext() != null) {
            Log.d(TAG, message);
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }
}

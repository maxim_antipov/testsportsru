package ru.antipov.maxim.testsportsrubasketball.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;

import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerBaseHelper;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema.FavoritesTable;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;

public class PlayerModel {
    public interface LoadCallback {
        void onLoad();
    }

    public void addFavoritePlayer(final Context context, final LoadCallback loadCallback, final int id_player) {
        SQLiteDatabase mDatabase = new PlayerBaseHelper(context).getWritableDatabase();
        ContentValues values = getContentFavoritePlayer(id_player);

        try {
            if(hasFavoritePlayer(mDatabase, id_player)) {
                mDatabase.insert(FavoritesTable.NAME, null, values);
            }
        } catch (SQLiteConstraintException e) {
            e.printStackTrace();
        }

        mDatabase.close();

        loadCallback.onLoad();
    }

    private ContentValues getContentFavoritePlayer(int id_player) {
        ContentValues values = new ContentValues();
        values.put(FavoritesTable.Cols.ID_PLAYER,  id_player);
        return values;
    }

    private boolean hasFavoritePlayer(SQLiteDatabase mDatabase, int id_player) {
        Cursor cursor = mDatabase.rawQuery("select * from " + FavoritesTable.NAME +" WHERE " + FavoritesTable.Cols.ID_PLAYER + " = " + Integer.toString(id_player) + ";",null);

        return cursor.getCount() == 0;
    }
}

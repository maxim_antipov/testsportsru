package ru.antipov.maxim.testsportsrubasketball.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema.FavoritesTable;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema.PlayerTable;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema.TeamTable;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;

public class PlayerBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "playerBase.db";

    public PlayerBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + PlayerTable.NAME + "("  +
                   PlayerTable.Cols.INDEX      + " integer primary key autoincrement, " +
                   PlayerTable.Cols.ID_PLAYER  + " integer, " +
                   PlayerTable.Cols.FIRST_NAME + ", " +
                   PlayerTable.Cols.LAST_NAME  + ", " +
                   PlayerTable.Cols.POSITION   + ", " +
                   PlayerTable.Cols.TEAM       + ")");

        db.execSQL("create table " + TeamTable.NAME + "(" +
                   TeamTable.Cols.ID           + " integer, " +
                   TeamTable.Cols.ABBREVIATION + ", " +
                   TeamTable.Cols.CITY         + "," +
                   TeamTable.Cols.CONFERENCE   + ", " +
                   TeamTable.Cols.DIVISION     + ", " +
                   TeamTable.Cols.FULL_NAME    + ", " +
                   TeamTable.Cols.NAME         + ")");

        db.execSQL("create table " + FavoritesTable.NAME + "(" +
                   "_id integer primary key autoincrement, " +
                   FavoritesTable.Cols.ID_PLAYER + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

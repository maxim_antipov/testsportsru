package ru.antipov.maxim.testsportsrubasketball.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerBaseHelper;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema.PlayerTable;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema.TeamTable;
import ru.antipov.maxim.testsportsrubasketball.DataBase.ReadDataBase;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.PlayerData;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Team;
import ru.antipov.maxim.testsportsrubasketball.SingletonNetwork;
import ru.antipov.maxim.testsportsrubasketball.Utils.Utils;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PlayersModel {
    public interface LoadCallback {
        void onLoad(List<Player> players);
        void onError(String error);
    }

    private List<Player> players;
    private int page = 1;
    private final int per_page = 50;
    private boolean isLoad = false;

    public PlayersModel() {
        players = new ArrayList<>();
    }

    private List<Player> getLastPage() {
        int firstIndex  = ((page - 2) * per_page);
        int secondIndex = firstIndex + per_page;

        return players.subList(firstIndex, secondIndex);
    }

    public void loadPlayers(final Context context, final LoadCallback loadCallback) {
        if(!Utils.hasConnection(context)) {
            String selection = PlayerTable.Cols.INDEX + "> ? AND " + PlayerTable.Cols.INDEX  +"<= ?";
            String[] args    = new String[] {Integer.toString((page - 1) * per_page), Integer.toString(page * per_page)};

            Observable.create(new ReadDataBase(context).readPlayer(selection, args))
                    .subscribeOn(Schedulers.io())
                    .onBackpressureBuffer()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Player>() {
                        @Override
                        public void onCompleted() {
                            if(isLoad) {
                                page++;
                                isLoad = false;
                                loadCallback.onLoad(getLastPage());
                            } else {
                                loadCallback.onError("NotData");
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadCallback.onError(e.toString());
                        }

                        @Override
                        public void onNext(Player player) {
                            isLoad = true;
                            players.add(player);
                        }
                    });

            return;
        }

        Observable<PlayerData> observable = SingletonNetwork.getSingleton().getPlayers(getPage(), getPerPage());
        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PlayerData>() {
                    @Override
                    public void onCompleted() {
                        if(isLoad) {
                            page++;
                            isLoad = false;
                            loadCallback.onLoad(getLastPage());
                        } else {
                            loadCallback.onError("NotData");
                            loadCallback.onLoad(null);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadCallback.onError(e.toString());
                    }

                    @Override
                    public void onNext(PlayerData playerData) {
                        isLoad = true;
                        players.addAll(playerData.getPlayer());

                        SQLiteDatabase mDatabase = new PlayerBaseHelper(context).getWritableDatabase();

                        for(int i = 0; i < playerData.getPlayer().size(); i++) {
                            ContentValues values_player = getContentPlayer(playerData.getPlayer().get(i));
                            ContentValues values_team   = getContentTeam(playerData.getPlayer().get(i).getTeam());
                            try {
                                if(hasPlayer(mDatabase, playerData.getPlayer().get(i).getId())) {
                                    mDatabase.insert(PlayerTable.NAME, null, values_player);
                                }
                                if(hasTeam(mDatabase, playerData.getPlayer().get(i).getTeam().getId())) {
                                    mDatabase.insert(TeamTable.NAME, null, values_team);
                                }
                            } catch (SQLiteConstraintException e) {
                                e.printStackTrace();
                            }
                        }

                        mDatabase.close();
                    }
                });
    }

    private ContentValues getContentPlayer(Player player) {
        ContentValues values = new ContentValues();
        values.put(PlayerTable.Cols.ID_PLAYER,  player.getId());
        values.put(PlayerTable.Cols.FIRST_NAME, player.getFirstName());
        values.put(PlayerTable.Cols.LAST_NAME,  player.getLastName());
        values.put(PlayerTable.Cols.POSITION,   player.getPosition());
        values.put(PlayerTable.Cols.TEAM,       player.getTeam().getId());

        return values;
    }

    private ContentValues getContentTeam(Team team) {
        ContentValues values = new ContentValues();
        values.put(TeamTable.Cols.ID,           team.getId());
        values.put(TeamTable.Cols.ABBREVIATION, team.getAbbreviation());
        values.put(TeamTable.Cols.CITY,         team.getCity());
        values.put(TeamTable.Cols.CONFERENCE,   team.getConference());
        values.put(TeamTable.Cols.DIVISION,     team.getDivision());
        values.put(TeamTable.Cols.FULL_NAME,    team.getFullName());
        values.put(TeamTable.Cols.NAME,         team.getName());

        return values;
    }

    private boolean hasPlayer(SQLiteDatabase mDatabase, int id_player) {
        Cursor cursor = mDatabase.rawQuery("select * from " + PlayerTable.NAME +" WHERE " + PlayerTable.Cols.ID_PLAYER + " = " + Integer.toString(id_player) + ";",null);

        return cursor.getCount() == 0;
    }

    private boolean hasTeam(SQLiteDatabase mDatabase, int id_team) {
        Cursor cursor = mDatabase.rawQuery("select * from " + TeamTable.NAME +" WHERE " + TeamTable.Cols.ID + " = " + Integer.toString(id_team) + ";",null);

        return cursor.getCount() == 0;
    }

    private String getPage() {
        return Integer.toString(page);
    }

    private String getPerPage() {
        return Integer.toString(per_page);
    }
}

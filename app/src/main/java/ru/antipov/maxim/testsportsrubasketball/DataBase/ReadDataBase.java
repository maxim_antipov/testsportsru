package ru.antipov.maxim.testsportsrubasketball.DataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema.FavoritesTable;
import ru.antipov.maxim.testsportsrubasketball.DataBase.PlayerDbSchema.PlayerTable;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Team;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

public class ReadDataBase {
    private SQLiteDatabase mDatabase;

    public ReadDataBase(Context context) {
        mDatabase = new PlayerBaseHelper(context).getWritableDatabase();
    }

    public Observable.OnSubscribe<Player> readFavoritesPlayer() {
        List<String> id_player = new ArrayList<>();

        Cursor cursor_favorites_player = mDatabase.query(
                FavoritesTable.NAME,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        cursor_favorites_player.moveToFirst();

        while (!cursor_favorites_player.isAfterLast()) {
            id_player.add(new Integer(cursor_favorites_player.getInt(cursor_favorites_player.getColumnIndex(FavoritesTable.Cols.ID_PLAYER))).toString());
            cursor_favorites_player.moveToNext();
        }

        String selection = "";
        String[] args = id_player.toArray(new String[id_player.size()]);

        for(int i = 0; i < id_player.size() - 1; i++) {
            selection += PlayerTable.Cols.ID_PLAYER + "= ? OR ";
        }

        selection += PlayerTable.Cols.ID_PLAYER + "= ?";

        Log.d("counting", "selection = " + selection);
        Log.d("counting", "args = " + Arrays.toString(args));

        return readPlayer(selection, args);
    }

    public Observable.OnSubscribe<Player> readPlayer(final String selection, final String[] args) {
        Observable.OnSubscribe<Player> onSubscribe = new Observable.OnSubscribe<Player>() {
            @Override
            public void call(Subscriber<? super Player> subscriber) {
                Cursor cursor_player = mDatabase.query(
                        PlayerTable.NAME,
                        null,
                        selection,
                        args,
                        null,
                        null,
                        null,
                        null
                );

                int i = 0;

                try {
                    cursor_player.moveToFirst();
                    while (!cursor_player.isAfterLast()) {
                        i++;

                        if (subscriber.isUnsubscribed()) {
                            return;
                        }

                        int id_player = cursor_player.getInt(cursor_player.getColumnIndex(PlayerTable.Cols.ID_PLAYER));
                        String first_name = cursor_player.getString(cursor_player.getColumnIndex(PlayerTable.Cols.FIRST_NAME));
                        String last_name = cursor_player.getString(cursor_player.getColumnIndex(PlayerTable.Cols.LAST_NAME));
                        String position = cursor_player.getString(cursor_player.getColumnIndex(PlayerTable.Cols.POSITION));
                        int id_team = cursor_player.getInt(cursor_player.getColumnIndex(PlayerTable.Cols.TEAM));

                        Cursor cursor_team = mDatabase.query(
                                PlayerDbSchema.TeamTable.NAME,
                                null,
                                PlayerDbSchema.TeamTable.Cols.ID + " = ?",
                                new String[]{Integer.toString(id_team)},
                                null,
                                null,
                                null,
                                null
                        );

                        Team team = new Team();

                        cursor_team.moveToFirst();

                        while (!cursor_team.isAfterLast()) {
                            String abbreviation = cursor_team.getString(cursor_team.getColumnIndex(PlayerDbSchema.TeamTable.Cols.ABBREVIATION));
                            String city = cursor_team.getString(cursor_team.getColumnIndex(PlayerDbSchema.TeamTable.Cols.CITY));
                            String conference = cursor_team.getString(cursor_team.getColumnIndex(PlayerDbSchema.TeamTable.Cols.CONFERENCE));
                            String division = cursor_team.getString(cursor_team.getColumnIndex(PlayerDbSchema.TeamTable.Cols.DIVISION));
                            String full_name = cursor_team.getString(cursor_team.getColumnIndex(PlayerDbSchema.TeamTable.Cols.FULL_NAME));
                            String name = cursor_team.getString(cursor_team.getColumnIndex(PlayerDbSchema.TeamTable.Cols.NAME));

                            team.setId(id_team);
                            team.setAbbreviation(abbreviation);
                            team.setCity(city);
                            team.setConference(conference);
                            team.setDivision(division);
                            team.setFullName(full_name);
                            team.setName(name);
                            cursor_team.moveToNext();
                        }

                        cursor_team.close();

                        Player player = new Player();
                        player.setId(id_player);
                        player.setFirstName(first_name);
                        player.setLastName(last_name);
                        player.setPosition(position);
                        player.setTeam(team);

                        subscriber.onNext(player);
                        cursor_player.moveToNext();
                        Log.d("counting", "i = " + i);
                    }

                    subscriber.onCompleted();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    cursor_player.close();
                    mDatabase.close();
                }
            }
        };

        return onSubscribe;
    }
}

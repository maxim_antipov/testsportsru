package ru.antipov.maxim.testsportsrubasketball.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;
import ru.antipov.maxim.testsportsrubasketball.Presenters.PresenterPlayer;
import ru.antipov.maxim.testsportsrubasketball.R;
import ru.antipov.maxim.testsportsrubasketball.Views.PlayerView;

public class PlayerFragment extends Fragment implements PlayerView {
    private static final String ARG_PLAYER = "PLAYER";

    @BindView(R.id.tvName)         TextView tvName;
    @BindView(R.id.tvPosition)     TextView tvPosition;
    @BindView(R.id.tvAbbreviation) TextView tvAbbreviation;
    @BindView(R.id.tvCity)         TextView tvCity;
    @BindView(R.id.tvConference)   TextView tvConference;
    @BindView(R.id.tvDivision)     TextView tvDivision;
    @BindView(R.id.tvFullName)     TextView tvFullName;
    @BindView(R.id.tvTeamName)     TextView tvTeamName;
    @BindView(R.id.btnAddFav)      Button btnAddFav;

    private Player player;
    private PresenterPlayer presenter;

    public static PlayerFragment newInstance(Player player) {
            Bundle args = new Bundle();
            args.putParcelable(ARG_PLAYER, player);

            PlayerFragment fragment = new PlayerFragment();
            fragment.setArguments(args);
            return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        player = (Player) getArguments().getParcelable(ARG_PLAYER);

        presenter = new PresenterPlayer();
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.player_fragment, container, false);
        ButterKnife.bind(this, v);

        tvName.setText("Full Name: " + player.getFirstName() + " " + player.getLastName());
        tvPosition.setText("Position: " + player.getPosition());
        tvAbbreviation.setText("Abbreviation: " + player.getTeam().getAbbreviation());
        tvCity.setText("City: " + player.getTeam().getCity());
        tvConference.setText("Conference: " + player.getTeam().getConference());
        tvDivision.setText("Division: " + player.getTeam().getDivision());
        tvFullName.setText("Team Full Name: " + player.getTeam().getFullName());
        tvTeamName.setText("Team Name: " + player.getTeam().getName());

        btnAddFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.addFavorites(getContext(), player.getId());
            }
        });

        return v;
    }

    @Override
    public void toastMessage(String message) {
        if(getActivity() != null) {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }
}

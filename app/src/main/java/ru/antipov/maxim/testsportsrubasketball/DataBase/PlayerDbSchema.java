package ru.antipov.maxim.testsportsrubasketball.DataBase;

public class PlayerDbSchema {
    public static final class PlayerTable {
        public static final String NAME = "player";

        public static final class Cols {
            public static final String INDEX      = "indx";
            public static final String ID_PLAYER  = "id_player";
            public static final String FIRST_NAME = "first_name";
            public static final String LAST_NAME  = "last_name";
            public static final String POSITION   = "position";
            public static final String TEAM       = "team";
        }
    }

    public static final class TeamTable {
        public static final String NAME = "team";

        public static final class Cols {
            public static final String ID           = "id_team";
            public static final String ABBREVIATION = "abbreviation";
            public static final String CITY         = "city";
            public static final String CONFERENCE   = "conference";
            public static final String DIVISION     = "division";
            public static final String FULL_NAME    = "full_name";
            public static final String NAME         = "name";
        }
    }

    public static final class FavoritesTable {
        public static final String NAME = "favorites";
        public static final class Cols {
            public static final String ID_PLAYER = "id_player";
        }
    }
}

package ru.antipov.maxim.testsportsrubasketball;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ru.antipov.maxim.testsportsrubasketball.Fragments.PlayersFragment;

public class PlayersBasketballActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return PlayersFragment.newInstance();
    }
}

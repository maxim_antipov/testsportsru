package ru.antipov.maxim.testsportsrubasketball.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.antipov.maxim.testsportsrubasketball.JSONClasses.Player;
import ru.antipov.maxim.testsportsrubasketball.R;

public class PlayerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private List<Player> players = new ArrayList<>();
    private boolean isLoading = false;
    private int position = 0;

    private Context context;

    public interface setOnClickListenerBtnMoreInfo {
        void onClickMoreInfo(Player player);
    }

    public interface setOnBeginLoading {
        void beginLoading();
    }

    private setOnClickListenerBtnMoreInfo setOnClick;
    private setOnBeginLoading setLoading;

    public void setOnClickListenerRVAdepter(setOnClickListenerBtnMoreInfo setOnClick) {
        this.setOnClick = setOnClick;
    }

    public void setOnBeginLoadingRVAdapter(setOnBeginLoading setLoading) {
        this.setLoading = setLoading;
    }

    public void addIndexPlayers(int index, List<Player> players) {
        this.players.addAll(index, players);
    }

    public void addPlayers(ArrayList<Player> players) {
        this.players.addAll(players);
    }

    public void clearPlayers() {
        players.clear();
    }

    public PlayerAdapter(List<Player> players, final RecyclerView rvItem, Context context) {
        this.players.addAll(players);
        this.context = context;

        rvItem.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = ((LinearLayoutManager) recyclerView.getLayoutManager()).getChildCount();
                int totalItemCount = ((LinearLayoutManager) recyclerView.getLayoutManager()).getItemCount();
                int firstVisibleItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

                if (!isLoading && setLoading != null) {
                    if (firstVisibleItems != 0 && (visibleItemCount + firstVisibleItems) >= totalItemCount) {
                        PlayerAdapter.this.players.add(null);

                        recyclerView.post(new Runnable() {
                            public void run() {
                                notifyItemInserted(PlayerAdapter.this.players.size() - 1);
                            }
                        });

                        isLoading = true;
                        setLoading.beginLoading();
                    }
                }
            }
        });
    }

    public void removeNull() {
        if (players.size() > 0) {
            isLoading = false;
            players.remove(players.size() - 1);
            notifyItemRemoved(players.size());
        }
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBarItemLoading);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cvPlayer)    public CardView cvPlayer;
        @BindView(R.id.tvFirstName) public TextView tvFirstName;
        @BindView(R.id.tvLastName)  public TextView tvLastName;
        @BindView(R.id.tvTeamName)  public TextView tvNameTeam;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            /*cvPlayer    = (CardView) itemView.findViewById(R.id.cvPlayer);
            tvFirstName = (TextView) itemView.findViewById(R.id.tvFirstName);
            tvLastName  = (TextView) itemView.findViewById(R.id.tvLastName);
            tvNameTeam  = (TextView) itemView.findViewById(R.id.tvTeamName);*/
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_player, parent, false);
            return new ItemViewHolder(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(v);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            itemViewHolder.tvFirstName.setText("First Name: " + players.get(position).getFirstName());
            itemViewHolder.tvLastName.setText("Last Name: "   + players.get(position).getLastName());
            itemViewHolder.tvNameTeam.setText("Team Name: "   + players.get(position).getTeam().getFullName());

            itemViewHolder.cvPlayer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setOnClick.onClickMoreInfo(new Player(players.get(position)));
                }
            });

            this.position = position;
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public int getItemViewType(int position) {
        return players.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return players == null ? 0 : players.size();
    }
}
